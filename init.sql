CREATE USER weather WITH ENCRYPTED PASSWORD 'mysecretpassword';

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO weather;

CREATE TABLE countries(
	id SMALLSERIAL,
	name VARCHAR(128) NOT NULL UNIQUE,
	PRIMARY KEY(id)
);

CREATE TABLE cities(
	id BIGSERIAL,
	name VARCHAR(128) NOT NULL UNIQUE,
	latitude REAL NOT NULL,
	longitude REAL NOT NULL,
	country_id SMALLINT NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_country FOREIGN KEY(country_id) REFERENCES countries(id)
);

CREATE TABLE weather_data(
	id BIGSERIAL,
	temperature REAL NOT NULL,
	pressure SMALLINT NOT NULL,
	humidity SMALLINT NOT NULL,
	city_id BIGINT NOT NULL,
	time_of_recording TIMESTAMP NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_city FOREIGN KEY(city_id) REFERENCES cities(id)
);

INSERT INTO countries(name) VALUES ('Ukraine'), ('France'), ('USA'), ('Japan');

INSERT INTO cities(name, latitude, longitude, country_id) VALUES ('Kyiv', 50.450001, 30.523333, (SELECT id FROM countries WHERE name = 'Ukraine')), ('Paris', 48.864716, 2.349014, (SELECT id FROM countries WHERE name = 'France')), ('New York', 40.730610, -73.935242, (SELECT id FROM countries WHERE name = 'USA')), ('Tokyo', 35.652832, 139.839478, (SELECT id FROM countries WHERE name = 'Japan'));