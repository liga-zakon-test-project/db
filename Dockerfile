FROM postgres:16.1

ENV POSTGRES_PASSWORD=password
ENV POSTGRES_DB=weatherdb

COPY init.sql /docker-entrypoint-initdb.d